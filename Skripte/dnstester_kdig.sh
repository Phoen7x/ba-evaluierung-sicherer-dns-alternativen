#!/bin/bash

##################################
# kdig für ungecachete Messungen #
##################################

set -u  # uninitialisierte Variablen abfangen

# Erste Variablen-Deklaration
called="$(basename "$0")"
count="$1"
batch_size="$2"
batch_increase="$3"
server_ip="$4"
declare -a domainnames["$(("$batch_size" - "1"))"] # Array mit Größe batch_size (- 1 da array mit 0 beginnt) erstellen
datum="$(date +'%F_%H-%M-%S')"
logfile="${called}_${server_ip}_${datum}.log"
listsize="$(wc -l < top-1m)"
max_batch="$(("$listsize" - "$batch_size"))" # batchsize von Anzahl a-records Zeilen abziehen, um overread zu vermeiden
#echo "list: $listsize maxbatch: $max_batch"
lokal="no"

# Prüfen, ob genau vier Parameter übergeben werden
if [ "$#" != "4" ]
then
  echo "Aborting: $# parameters is more or less than the four expected ones!" >&2
  exit 1
fi

# Prüfen ob count kleiner Anzahl Domains
if [ "$count" -lt "$batch_size" ]
then
  echo "Aborting: $count is greater then $batch_size!" >&2
  exit 1
# Prüfen ob Count zwischen 1 und größe der Liste liegt
elif [ "$count" -lt "1" ] && [ "$count" -gt "$listsize" ]
then
  echo "Aborting: $count is out of range!" >&2
  exit 1
fi

# Logfile erstellen sofern keine Fehler auftreten
if ! touch "$logfile"
then
  echo "Aborting: Failed to create logfile!" >&2
  exit 1
fi

# Prüfen ob lokaler Resolver, dann Cache leeren und Variable setzen.
case "$server_ip" in
192.168.108.91)
  resolver="dns-bind9"
  ssh -i /home/dnstester/.ssh/$resolver root@$server_ip "/etc/init.d/bind9 restart"
  lokal="yes"
  ;;
192.168.108.92)
  resolver="dns-unbound"
  ssh -i /home/dnstester/.ssh/$resolver root@$server_ip "/etc/init.d/unbound restart"
  lokal="yes"
  ;;
192.168.108.93)
  resolver="dns-knot"
  ssh -i /home/dnstester/.ssh/$resolver root@$server_ip "systemctl restart kresd@{1..4}.service"
  lokal="yes"
  ;;
esac
sleep 3 # 3 Sek. warten bis Dienst neugestartet ist.

# Zweite Variablen-Deklaration
current_line=0
current_batch=0
remote_measurement="measure.sh"

# Inhalt aus der Abfragendatei lesen und in einer Schleife ausführen
while [ "$current_line" -lt "$count" ] && [ "$current_line" -le "$max_batch" ] # solange current_line < Anzahl und <= max_batch  
do
  current_batch="$(("$current_batch" + "1"))"
  echo "Batch $current_batch" >> "$logfile"
  batch_start="$(("$current_line" + "1"))"
  batch_end="$(("$(("$batch_start" + "$batch_size"))" - "1"))"
  echo "Start: $batch_start Ende: $batch_end"
  echo "$(sed -n "${batch_start},${batch_end}p" top-1m)"
  sed -n "${batch_start},${batch_end}p" top-1m > top-1m.tmp
  read -r -d '' -a domainnames < top-1m.tmp  
  rm top-1m.tmp

  if [ "$lokal" = "yes" ] 
  then
    ssh -i /home/dnstester/.ssh/$resolver root@$server_ip /usr/local/bin/$remote_measurement $datum $batch_size &
    sleep 3
  fi

  j=0
  while [ "$j" -lt "$batch_size" ] 
  do
    echo "${domainnames["$j"]}"
    domname="${domainnames["$j"]}"
    current_line="$(("$current_line" + "1"))"
    echo "$batch_size;$(kdig @$server_ip "$domname" +tls | tail -1 | cut -d " " -f 5)" >> "$logfile" &
    j="$(("$j" + "1"))"
  done
  batch_size=$(("$batch_size" + "$batch_increase"))
  max_batch="$(("$count" - "$batch_size"))" # batchsize von Anzahl a-records Zeilen abziehen, um overread zu vermeiden

  if [ "$lokal" = "yes" ]  
  then
    ssh -i /home/dnstester/.ssh/$resolver root@$server_ip pkill $remote_measurement
  fi
  sleep 6  # warten dass alle Anfragen des aktuellen Batches angekommen sind
done 

# remote Messung abholen
if [ "$lokal" = "yes" ]  
then
  scp root@${server_ip}:/usr/local/bin/${remote_measurement}_${datum}.log ./
fi
