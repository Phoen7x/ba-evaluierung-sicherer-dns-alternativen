#!/bin/bash

################################
# kdig für gecachete Messungen #
################################

set -u  # uninitialisierte Variablen abfangen

# Erste Variablen-Deklaration
called="$(basename "$0")"
count="$1"
batch_size="$2"
batch_increase="$3"
server_ip="$4"
datum="$(date +'%F_%H-%M-%S')"
logfile="${called}_${server_ip}_${datum}.log"
lokal="no"

# Prüfen, ob genau vier Parameter übergeben werden
if [ "$#" != "4" ]
then
  echo "Aborting: $# parameters is more or less than the four expected ones!" >&2
  exit 1
fi

# Prüfen ob count kleiner Batchsize
if [ "$count" -lt "$batch_size" ]
then
  echo "Aborting: $count is greater then $batch_size!" >&2
  exit 1
# Prüfen ob Count zwischen 1 und 10.000 liegt
elif [ "$count" -lt "1" ] && [ "$count" -gt "10000" ]
then
  echo "Aborting: $count is out of range!" >&2
  exit 1
fi

# Logfile erstellen sofern keine Fehler auftreten
if ! touch "$logfile"
then
  echo "Aborting: Failed to create logfile!" >&2
  exit 1
fi

# Prüfen ob lokaler Resolver, dann Variable setzen.
case "$server_ip" in
192.168.108.91)
  resolver="dns-bind9"
  lokal="yes"
  ;;
192.168.108.92)
  resolver="dns-unbound"
  lokal="yes"
  ;;
192.168.108.93)
  resolver="dns-knot"
  lokal="yes"
  ;;
esac

# Zweite Variablen-Deklaration
current_line=0
current_batch=0
max_batch="$(("$count" - "$batch_size"))" # batchsize von Anzahl A-Records Zeilen abziehen, um overread zu vermeiden
remote_script="measure.sh"


# Inhalt aus der Abfragendatei lesen und in einer Schleife ausführen
while [ "$current_line" -lt "$count" ] && [ "$current_line" -le "$max_batch" ] # solange current_line < Anzahl und <= max_batch  
do
  current_batch="$(("$current_batch" + "1"))"
  #echo "Batch $current_batch" >> "$logfile"
  batch_start="$(("$current_line" + "1"))"
  batch_end="$(("$(("$batch_start" + "$batch_size"))" - "1"))"
  echo "Start: $batch_start Ende: $batch_end"
  
  # remote Messung anstoßen
  if [ "$lokal" = "yes" ]
  then
    ssh -i /home/dnstester/.ssh/$resolver root@$server_ip /usr/local/bin/$remote_script $datum $batch_size &
    sleep 5 # idle-Zustand erfassen
  fi
  
  j=0
  while [ "$j" -lt "$batch_size" ] 
  do
    current_line="$(("$current_line" + "1"))"
    echo "$batch_size;$(kdig @$server_ip dns-testumgebung.lan +tls | tail -1 | cut -d " " -f 5)" >> "$logfile" &
    j="$(("$j" + "1"))"
  done
 
  if [ "$lokal" = "yes" ]
  then
    ssh -i /home/dnstester/.ssh/$resolver root@$server_ip pkill $remote_script
  fi
  batch_size=$(("$batch_size" + "$batch_increase"))
  max_batch="$(("$count" - "$batch_size"))" # batchsize von Anzahl a-records Zeilen abziehen, um overread zu vermeiden
  sleep 3 
done

# remote Messung abholen
if [ "$lokal" = "yes" ]
then
  scp root@${server_ip}:/usr/local/bin/${remote_script}_${datum}.log ./
fi
